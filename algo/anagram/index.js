/*
Anagram
Miles Marshall 05 Mar 2021
*/
console.clear();

//var str1 = "dog";
//var str2 = "GOD";

//var str1 = "foo";
//var str2 = "poo";

//var str1 = "a man with a plan";
//var str2 = "with a plan a man";

function checkAna(str1, str2) {
  // convert to lowercase
  // then convert to an array to sort on
  // then convert back to string
  //s1 = str1.toLowerCase().replace(/[^a-z]/g, '').split('').sort().join(''); // only alpha chars ?
  //s2 = str2.toLowerCase().replace(/[^a-z]/g, '').split('').sort().join('');
  s1 = str1.toLowerCase().split('').sort().join('');
  s2 = str2.toLowerCase().split('').sort().join('');

  if (s1 === s2)
    return true;
  else
   return false;
}

function go() {
  // 1
  var str1 = "";
  var str2 = "ABC";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 2
  var str1 = "ABC";
  var str2 = "";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 3
  var str1 = "Dormitory";
  var str2 = "Dirty_room";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 4
  var str1 = "Funeral";
  var str2 = "Reel fun";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 5
  var str1 = "School master?!";
  var str2 = "!?The classroom";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 6
  var str1 = "Listen";
  var str2 = "Silence";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;

  // 7
  var str1 = "Funeral";
  var str2 = "Real fun";

  var check = checkAna(str1, str2);
  var output = '<p>is "'+str1+'" an anagram of "'+str2+'"? <b>that is '+check+'</b></p>';
  document.getElementById("output").innerHTML += output;
}

module.exports = { checkAna };