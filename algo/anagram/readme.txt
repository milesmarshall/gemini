Miles Marshall
05 Mar 2021


## Anagram
----------------

Run the test :
1) click Go
2) In console 'npm test'

Should get the following:

Test Suites: 1 passed, 1 total
Tests:       7 passed, 7 total
Snapshots:   0 total
Time:        2.061 s


## Anagram Tests
----------------

[Test()]
public void Word1_IsRequired()
{
  Assert.Throws<ArgumentException>(() => _anagram.AreAnagrams(string.Empty, "ABC"));
}

[Test()]
public void Word2_IsRequired()
{
  Assert.Throws<ArgumentException>(() => _anagram.AreAnagrams("ABC", string.Empty));
}

[Test()]
public void Dormitory_IsAnagram_Dirty_room()
{
  var result = _anagram.AreAnagrams("Dormitory", "Dirty_room");

  Assert.IsTrue(result);
}

[Test()]
public void Funeral_IsAnagram_Reel_fun()
{
  var result = _anagram.AreAnagrams("Funeral", "Reel fun");

  Assert.IsTrue(result);
}

[Test()]
public void School_master_IsAnagram_The_classroom()
{
  var result = _anagram.AreAnagrams("School master?!", "!?The classroom");

  Assert.IsTrue(result);
}

[Test()]
public void Listen_Is_NOT_Anagram_Silence()
{
  var result = _anagram.AreAnagrams("Listen", "Silence");

  Assert.IsFalse(result);
}

[Test()]
public void Funeral_IsAnagram_Real_fun()
{
  var result = _anagram.AreAnagrams("Funeral", "Real fun");

  Assert.IsTrue(result);
}