/*
Anagram tests
Miles Marshall 05 Mar 2021
*/

/*
1. string.Empty, "ABC"
2. "ABC", string.Empty
3. "Dormitory", "Dirty_room"
4. "Funeral", "Reel fun"
5. "School master?!", "!?The classroom"
6. "Listen", "Silence"
7. "Funeral", "Real fun"
*/

const { checkAna } = require('./index');


test("'' an anagram of 'ABC' = FALSE", () => {
  expect(checkAna('', 'ABC')).toBe(false);
});

test("'ABC' an anagram of '' = FALSE", () => {
  expect(checkAna('ABC', '')).toBe(false);
});

test("'Dormitory' an anagram of 'Dirty_room' = FALSE", () => {
  expect(checkAna('Dormitory', 'Dirty_room')).toBe(false);
});

test("'Funeral' an anagram of 'Reel fun' = FALSE", () => {
  expect(checkAna('Funeral', 'Reel fun')).toBe(false);
});

test("'School master?!' an anagram of '!?The classroom' = TRUE", () => {
  expect(checkAna('School master?!', '!?The classroom')).toBe(true);
});

test("'Listen' an anagram of 'Silence' = FALSE", () => {
  expect(checkAna('Listen', 'Silence')).toBe(false);
});

test("'Funeral' an anagram of 'Real fun' = FALSE", () => {
  expect(checkAna('Funeral', 'Real fun')).toBe(false);
});