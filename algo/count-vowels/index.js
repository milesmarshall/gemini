/*
Count Vowels
Miles Marshall 05 Mar 2021
*/
console.clear();

//var str = "miles";    //2
//var str = "help";     //1
//var str = "ggg";      //0
//var str = "hoolahoop";  //5

const countVowels = function(str) {
  var count = 0;

  ('' + str).split('').map(function(v, i) {
    if (vowelTest(v))
      count++;
  })
  return count;
};

function vowelTest(s) {
  return (/^[aeiou]$/i).test(s); // preferring regex over array of items for simplicity but not sure which would be faster
}

function go() {

  // 1
  var str = "";
  var x = countVowels(str);
  var output = '<p>I am guessing there are <b>' + x + ' vowels</b> in the word "' + str + '"</p>';
  document.getElementById("output").innerHTML += output;

  // 2
  var str = "AEIOU";
  var x = countVowels(str);
  var output = '<p>I am guessing there are <b>' + x + ' vowels</b> in the word "' + str + '"</p>';
  document.getElementById("output").innerHTML += output;

  // 3
  var str = "lmnpqr";
  var x = countVowels(str);
  var output = '<p>I am guessing there are <b>' + x + ' vowels</b> in the word "' + str + '"</p>';
  document.getElementById("output").innerHTML += output;

  // 4
  var str = "abcdefghijklmnopqrstuvwxyz";
  var x = countVowels(str);
  var output = '<p>I am guessing there are <b>' + x + ' vowels</b> in the word "' + str + '"</p>';
  document.getElementById("output").innerHTML += output;

  // 5
  var str = "How many can you find";
  var x = countVowels(str);
  var output = '<p>I am guessing there are <b>' + x + ' vowels</b> in the word "' + str + '"</p>';
  document.getElementById("output").innerHTML += output;
}

module.exports = { countVowels };