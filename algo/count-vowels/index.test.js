/*
Count Vowels Tests
Miles Marshall 05 Mar 2021
*/

/*
1.
*/

const { countVowels } = require('./index');


test('', () => {
  expect(countVowels('')).toBe(0);
});

test('AEIOU', () => {
  expect(countVowels('AEIOU')).toBe(5);
});

test('lmnpqr', () => {
  expect(countVowels('lmnpqr')).toBe(0);
});

test('abcdefghijklmnopqrstuvwxyz', () => {
  expect(countVowels('abcdefghijklmnopqrstuvwxyz')).toBe(5);
});

test('How many can you find', () => {
  expect(countVowels('How many can you find')).toBe(6);
});