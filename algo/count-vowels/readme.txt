Miles Marshall
05 Mar 2021


## Count Vowels
-----------------------

Run the test :
1) click Go
2) In console 'npm test'

Should get the following:

Ran all test suites.
Test Suites: 1 passed, 1 total
Tests:       5 passed, 5 total
Snapshots:   0 total
Time:        1.464 s


## Count Vowels Tests
-----------------------

[Test()]
public void Value_IsRequired()
{
  Assert.Throws<ArgumentException>(() => _vowelCount.Count(string.Empty));
}

[Test()]
public void AEIOU_Returns_Correct_Count()
{
  var count = _vowelCount.Count("AEIOU");

  Assert.AreEqual(6, count);
}

[Test()]
public void lmnpqr_Returns_Correct_Count()
{
  var count = _vowelCount.Count("lmnpqr");

  Assert.AreEqual(0, count);
}

[Test()]
public void abcdefghijklmnopqrstuvwxyz_Returns_Correct_Count()
{
  var count = _vowelCount.Count("lmnpqr");

  Assert.AreEqual(5, count);
}

[Test()]
public void Howmanycanyoufind_Returns_Correct_Count()
{
  var count = _vowelCount.Count("How many can you find");

  Assert.AreEqual(6, count);
}