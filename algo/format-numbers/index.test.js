/*
Format Numbers Tests
Miles Marshall 05 Mar 2021
*/

/*
-1 would be formatted as -1

1000000001 would be formatted as 1,000,000,001

1 would be formatted as 1

10 would be formatted as 10

100 would be formatted as 100

1000 would be formatted as 1,000

10000 would be formatted as 10,000

100000 would be formatted as 100,000

1000000 would be formatted as 1,000,000

35235235 would be formatted as 35,235,235
*/

const { formatNumber } = require('./index');


test("-1 should be formatted as -1", () => {
  expect(formatNumber('-1')).toBe('-1');
});

test("1000000001 should be formatted as 1,000,000,001", () => {
  expect(formatNumber('1000000001')).toBe('1,000,000,001');
});

test("1 should be formatted as 1", () => {
  expect(formatNumber('1')).toBe('1');
});

test("10 should be formatted as 10", () => {
  expect(formatNumber('10')).toBe('10');
});

test("-1 should be formatted as -1", () => {
  expect(formatNumber('-1')).toBe('-1');
});

test("100 should be formatted as 100", () => {
  expect(formatNumber('100')).toBe('100');
});

test("1000 should be formatted as 1,000", () => {
  expect(formatNumber('1000')).toBe('1,000');
});

test("10000 should be formatted as 10,000", () => {
  expect(formatNumber('10000')).toBe('10,000');
});

test("100000 should be formatted as 100,000", () => {
  expect(formatNumber('100000')).toBe('100,000');
});

test("1000000 should be formatted as 1,000,000", () => {
  expect(formatNumber('1000000')).toBe('1,000,000');
});

test("35235235 should be formatted as 35,235,235", () => {
  expect(formatNumber('35235235')).toBe('35,235,235');
});


