Miles Marshall
05 Mar 2021


## Format Numbers
-----------------------

Run the test :
1) click Go
2) In console 'npm test'

Should get the following:

Test Suites: 1 passed, 1 total
Tests:       11 passed, 11 total
Snapshots:   0 total
Time:        1.706 s



## Format Numbers Tests
-----------------------

[Test()]
public void NegativeNumber_Throws_ArgumentOutOfRangeException()
{
  Assert.Throws<ArgumentOutOfRangeException>(() => _formatNumber.Format(-1));
}

[Test()]
public void BiggerThanMaxNumber_Throws_ArgumentOutOfRangeException()
{
  Assert.Throws<ArgumentOutOfRangeException>(() => _formatNumber.Format(1000000001));
}

[Test()]
public void Value_1_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("1", formatted);
}

[Test()]
public void Value_10_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("10", formatted);
}

[Test()]
public void Value_100_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("1,00", formatted);
}

[Test()]
public void Value_1000_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("1,000", formatted);
}

[Test()]
public void Value_10000_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("10,000", formatted);
}

[Test()]
public void Value_100000_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("100,000", formatted);
}

[Test()]
public void Value_1000000_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("1,000,000", formatted);
}

[Test()]
public void Value_35235235_Returns_ValidString()
{
  var formatted = _formatNumber.Format(1);

  Assert.AreEqual("35,2352,35", formatted);
}