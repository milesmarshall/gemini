/*
Format Numbers
Miles Marshall 05 Mar 2021
*/
console.clear();

function formatNumber(num) {
  var arr = [];
  var rev = [];

  // lets split up that number, convert to ascii and add to array
  ('' + num).split('').map(function(v, i) {
    arr.push(v.charCodeAt(0));
  })

  // reverse array, and at every 4th character insert comma
  var rev = arr.reverse();
  var pos = 0;
  var interval = 4;

  while (pos < rev.length) {
    rev.splice(pos, 0, 44);
    pos += interval;
  }

  // knock the leading comma off the array
  rev.shift();

  // reserve order again
  arr = rev.reverse().join('');

  // ok so lets convert the code back to UTF, maybe a bit crude and assuming
  var codes = [];

  for (let i = 0; i < arr.length;) {
    const digits = arr[i] === '1' ? 3 : 2;
    codes.push(arr.substr(i, digits));
    i += digits;
  }

  return String.fromCharCode(...codes);
}


function go() {
  // 1
  var num = -1;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 2
  var num = 1000000001;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 3
  var num = 1;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 4
  var num = 10;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 5
  var num = 100;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 6
  var num = 1000;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 7
  var num = 10000;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 8
  var num = 100000;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 9
  var num = 1000000;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;

  // 10
  var num = 35235235;
  var formatted = formatNumber(num);
  var output = '<p>' + num + ' would be formatted as ' + formatted + '</p>';
  document.getElementById("output").innerHTML += output;
}


module.exports = { formatNumber };