# ToDo

Miles Marshall

## Implementation

1. Started with initial boilerplate for react, and added libraries for routing (nav), redux (state)
2. Built initial HTML components for each section
  - main app
  - todo main
    - nav
    - input component
    - list component
3. Setup some variabes/constants
4. Setup actions
5. Setup store
6. Plugged in event functionality & state usage with reducers
7. Setup libraries for JWT
8. Building services, reducers & actions
9. Came across some issues with backend (see issues 1 & 2 below)
10. Once passed that, was able to register & login (token created, user created etc)
11.


## Pending

1. Checkbox for todo is acting inverted, need to pass the current state of checkbox as well as the ID (or invert on update)
2. Form validation
3. Clear state on hard reset


## Issues

1. User object not reaching model, having to hard-code in the meantime required params
2. secret key is not being picked up from process.env - something missing there? having to hard-code to get around
3. Using localStorage to initially hold user details post registration because none of this is returned from the payload, then populating state post login


##
1. could have tried with ejs ?



curl --verbose --request POST --data "password=miles" http://bcrypt.org/api/generate-hash.json

miles
7
$2b$07$rfFGBGpqZwN70LSSfvmG.OA7bn6tdWowcf0.SoN7n/P9NILFbb3KO


