import React from "react";
import { NavLink } from 'react-router-dom';

const Header = () => {
  return <div className="nav">
    <NavLink to="/todo">Todo</NavLink>
    <NavLink to="/register">Register</NavLink>
    <NavLink to="/login">Log In</NavLink>
    <NavLink to="/logout">Log Out</NavLink>
    <NavLink to="/user">User</NavLink>
    <NavLink to="/test">Test</NavLink>
  </div>;
};

export default Header;
