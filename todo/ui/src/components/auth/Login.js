import React from 'react';
import InputField from './InputField';

import { connect } from "react-redux";
import { login } from "../../actions/auth";

class Login extends React.Component {

  constructor(props) {
    super(props)

    this.handleLogin = this.handleLogin.bind(this);

    this.state = {
      name: "",
      email: "",
      password: "",
      successful: false,
    }
  }

  setInputValue(property, val) {
    val = val.trim()
    this.setState({
      [property]: val
    })
  }

  handleLogin(e) {
    e.preventDefault();

    if (!this.state.password) {
      return;
    }
    if (!this.state.email) {
      return;
    }

    this.setState({
      successful: false,
    });

    this.props
      .dispatch(
        login(this.state.email, this.state.password)
      )
      .then(() => {
        console.log('success');
        console.log(`state.email: ${this.state.email} state.password:  ${this.state.password} token: ${this.props.token}`);

        this.setState({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          successful: true,
          token: this.state.token,
        });
      })
      .catch(() => {
        console.log('fail');
        this.setState({
          successful: false,
        });
      });
  }

  render() {
    const { message } = this.props;
    const { user } = this.props;

    return (
      <div className="loginForm">
        <form onSubmit={this.handleLogin}>
          <InputField
            type='text'
            placeholder='Email'
            onChange={ (val) => this.setInputValue('email', val) }
          />
          <InputField
            type='password'
            placeholder='Password'
            onChange={ (val) => this.setInputValue('password', val) }
          />
          <input type="submit" value="Login" />
        </form>

        {user && (
          <p>{user.name} {user.email} {user.password} {user.token}</p>
        )}

        {message && (
          <p>
            {message}
          </p>
        )}
      </div>
    );
  }

}

function mapStateToProps(state) {
  const { message } = state.message;
  const { user } = state.auth;
  return {
    message,
    user
  };
}

export default connect(mapStateToProps)(Login);