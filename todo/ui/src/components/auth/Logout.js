import React from 'react';
import UserStore from '../../store/index'
import SubmitButton from './SubmitButton';

class Logout extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      buttonDisabled:  true
    }
  }

  resetForm() {
    this.setState({
      buttonDisabled:  true
    })
  }

  async doLogout() {
    UserStore.isLoggedIn = false;
    UserStore.username = null;
    return;

    /*try {
      let res = await fetch('/login', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({

        })
      });

      let result = await res.json()
      if (result && result.success) {
        UserStore.isLoggedIn = false
        UserStore.username = null
      }
      else if (result && result.success === false) {
        this.resetForm()
        alert(result.msg)
      }
    }

    catch(e) {
      console.log(e)
      this.resetForm()
    }*/
  }

  render() {
    return (
      <div className="logoutForm">
        <SubmitButton
          text='confirm logout'
          onClick={ () => this.doLogout() }
        />
      </div>
    );
  }

}

export default Logout;