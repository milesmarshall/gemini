import React from 'react';

import { connect } from "react-redux";
import { get } from "../../actions/auth";

class User extends React.Component {

  constructor(props) {
    super(props)

    this.handleGetUser = this.handleGetUser.bind(this);

    this.state = {
      name: "",
      email: "",
      password: "",
      successful: false,
    }
  }

  handleGetUser() {
    console.log('handleGetUser');
    this.props
      .dispatch(
        get()
      )
      .then(() => {
        console.log('success');

        this.setState({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          successful: true,
          token: this.state.token,
        });

        console.log(this.state);
      })
      .catch(() => {
        console.log('fail');
      });

  }

  render() {
    const { message } = this.props;
    const { user } = this.props;

    return (
      <div className="userForm">
        {user && (
          <p>{user.name} {user.email} {user.password} {user.token}</p>
        )}

        {message && (
          <p>
            {message}
          </p>
        )}
      </div>
    );
  }

}

function mapStateToProps(state) {
  const { message } = state.message;
  const { user } = state.auth;
  return {
    message,
    user
  };
}

export default connect(mapStateToProps)(User);