import React from 'react';
import InputField from './InputField';

import { connect } from "react-redux";
import { register } from "../../actions/auth";

class Register extends React.Component {

  constructor(props) {
    super(props)

    this.handleRegister = this.handleRegister.bind(this);

    this.state = {
      name: "",
      email: "",
      password: "",
      successful: false,
    }
  }

  setInputValue(property, val) {
    val = val.trim()
    this.setState({
      [property]: val
    })
  }

  handleRegister(e) {
    e.preventDefault();

    if (!this.state.name) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    if (!this.state.email) {
      return;
    }

    this.setState({
      successful: false,
    });

    console.log('register');

    this.props
      .dispatch(
        register(this.state.name, this.state.email, this.state.password)
      )
      .then(() => {
        this.setState({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          token: this.props.message,
        });
        this.setState({
          successful: true,
        });

        localStorage.setItem("user", JSON.stringify({
          "name": this.state.name,
          "password": this.state.password,
          "email": this.state.email,
          "token": this.props.message,
        }));
      })
      .catch(() => {
        this.setState({
          successful: false,
        });
      });
  }

  /*testRegister() {
    fetch('/register', {
      method: "POST",
      body: JSON.stringify(this.state)
    }).then ((response) => {
      response.json().then((result) => {
        console.warn('result', result);
        localStorage.setItem('register', JSON.stringify({
          successful: true,
          token: result.token
        }))
      })
    })
  }*/

  render() {
    const { message } = this.props;

    return (
      <div className="registerForm">
        <form onSubmit={this.handleRegister}>
          <InputField
            type='text'
            placeholder='Name'
            onChange={ (val) => this.setInputValue('name', val) }
          />
          <InputField
            type='text'
            placeholder='Email'
            onChange={ (val) => this.setInputValue('email', val) }
          />
          <InputField
            type='password'
            placeholder='Password'
            onChange={ (val) => this.setInputValue('password', val) }
          />
          <input type="submit" value="Register" />
        </form>

        {message && (
          <div className="" role="alert">
            {message}
          </div>
        )}
      </div>
    );
  }

}

function mapStateToProps(state) {
  const { message } = state.message;
  return {
    message,
  };
}

export default connect(mapStateToProps)(Register);