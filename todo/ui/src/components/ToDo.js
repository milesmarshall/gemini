import React from 'react';
import { connect } from "react-redux";
import List from "./List";
import Input from "./Input";

import { addTodo, deleteTodo, updateTodo, listTodo } from "../actions/todo";

class ToDo extends React.Component {

  constructor(props) {
    super(props)

    this.listTodo = this.onList.bind(this);
  }

  onChange = ({ target }) => {
    this.setState({ [target.name]: target.value, "completed":"false" });
  };

  onAdd = () => {
    //let id = Math.floor(Math.random() * 1000);
    //this.props.addTodo({ id, ...this.state });

    const user = {
      "name": this.props.user.name,
      "password": this.props.user.password,
      "email": this.props.user.email,
    };

    const task = {
      "task": this.state.task,
    };

    console.log(task);
    console.log(user);

    this.props
      .dispatch(
        addTodo(task, user)
      )
      .then(() => {
        console.log('add success');
      })
      .catch(() => {
        console.log('add fail');
      });
  };

  onDelete = (id) => {
    this.props.deleteTodo(id);
  };

  onChkChange = (id) => {
    this.props.updateTodo(id);
  }

  onList = () => {
    console.log('onlist');

    this.props
      .dispatch(
        listTodo('me@home.com', '12345')
      )
      .then(() => {
        console.log('list success');
        /*console.log(`state.email: ${this.state.email} state.password:  ${this.state.password} token: ${this.props.token}`);

        this.setState({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          successful: true,
          token: this.state.token,
        });*/
      })
      .catch(() => {
        console.log('list fail');
        /*this.setState({
          successful: false,
        });*/
      });

    //this.props.listTodo({ ...this.state });
  };

  render() {
    const { todos } = this.props;
    const { user } = this.props;

    return (
      <div className="">
        <button onClick={() => this.onList()}>
          Get
        </button>

        <Input
            onChange={(e) => this.onChange(e)}
            onAdd={() => this.onAdd()}
        />
        <List
          todos={todos}
          onDelete={(id) => this.onDelete(id)}
          onChkChange={(id) => this.onChkChange(id)}
        />
        <p>{user.name} {user.email} {user.password} {user.token}</p>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  const { todos } = state.todo;
  const { user } = state.auth;
  return {
    todos,
    user
  };
};

//export default connect(mapStateToProps, { addTodo, deleteTodo, updateTodo, listTodo })(ToDo);

export default connect(mapStateToProps)(ToDo);