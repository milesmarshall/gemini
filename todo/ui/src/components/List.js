import React from "react";

const List = ({ todos, onDelete, onChkChange }) => {
  return (
    /*
    TODOs: id, task, completed
    */
    <div className="todo-list">
      {todos && todos.length > 0 ? (
        todos.map((todo) => (
          <div className="todo-item" key={todo.id}>
            <input
              id={`checkbox-${todo.id}`}
              type="checkbox"
              value={todo.completed ? false : true}
              onChange={() => onChkChange(todo.id)}
              checked={todo.completed ? false : true}
            />
            <p>{todo.task}</p>
            <button className="" onClick={() => onDelete(todo.id)}>x</button>
          </div>
        ))
      ) : (
        <i>&nbsp;</i>
      )}
    </div>
  );
};

export default List;
