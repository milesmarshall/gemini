import React from "react";

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ToDo from "./ToDo";
import Register from "./auth/Register";
import Login from "./auth/Login";
import Logout from "./auth/Logout";
import User from "./auth/User";
import Test from "./Test";
import Nav from "./Nav";

class App extends React.Component {

  render() {
    return (
      <>
        <BrowserRouter>
          <div>
          <Nav />
            <Switch>
              <Route path="/todo" component={ToDo} exact/>
              <Route path="/register" component={Register} exact/>
              <Route path="/login" component={Login} exact/>
              <Route path="/logout" component={Logout} exact/>
              <Route path="/user" component={User} exact/>
              <Route path="/test" component={Test} exact/>
            </Switch>
          </div>
        </BrowserRouter>
      </>
    );
  }
}

export default App;