import { REGISTER_SUCCESS, REGISTER_FAIL, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, SET_MESSAGE, GET_USER } from "../constants/actions";

import AuthService from "../services/auth.service";

export const register = (name, email, password) => (dispatch) => {
  console.log(`auth.register: ${name} ${email} ${password}`);
  return AuthService.register(name, email, password).then(
    (response) => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: response.data
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.data.token,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REGISTER_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const login = (email, password) => (dispatch) => {
  return AuthService.login(email, password).then(
    (data) => {
      dispatch({
        type: SET_MESSAGE,
        payload: "Success",
      });

      dispatch({
        type: LOGIN_SUCCESS,
        payload: { auth: data },
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOGIN_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const get = () => (dispatch) => {
  dispatch({
    type: GET_USER,
    payload: 'ok',
  });
  return Promise.resolve();
};

export const logout = () => (dispatch) => {
  AuthService.logout();

  dispatch({
    type: LOGOUT,
  });
};