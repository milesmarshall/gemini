import { ADD_TODO, DELETE_TODO, UPDATE_TODO, LIST_TODO, SET_MESSAGE } from "../constants/actions";

import TodoService from "../services/todo.service";

export const addTodo = (task, user) => (dispatch) => {
  return TodoService.create(task, user).then(
    (data) => {
      dispatch({
        type: SET_MESSAGE,
        payload: "Success",
      });

      dispatch({
        type: ADD_TODO,
        payload: data,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

/*export const addTodo = (data) => ({
  type: ADD_TODO,
  payload: data,
});*/

export const deleteTodo = (id) => ({
  type: DELETE_TODO,
  payload: id,
});

export const updateTodo = (id) => ({
  type: UPDATE_TODO,
  payload: id,
});

export const listTodo = (user) => (dispatch) => {
  return TodoService.list(user).then(
    (data) => {
      dispatch({
        type: SET_MESSAGE,
        payload: "Success",
      });

      /*dispatch({
        type: LOGIN_SUCCESS,
        payload: { auth: data },
      });*/

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      /*dispatch({
        type: LOGIN_FAIL,
      });*/

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};