import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";

/** PROBLEM
should be able to import the index combined reducer file, but when doing so, the UI is not updated from the store
**/

//import rootReducer from "../reducers/todo";

import rootReducer from "../reducers/index";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = [thunk];
const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(...middleware))
);

export default store;

/*import rootReducer from "../reducers/todo";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancer());

export default store;*/