import { combineReducers } from "redux";
import todo from "./todo";
import auth from "./auth";
import message from "./message";

export default combineReducers({
  todo,
  auth,
  message,
});
