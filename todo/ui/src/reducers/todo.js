import { ADD_TODO, DELETE_TODO, UPDATE_TODO, LIST_SUCCESS, LIST_FAIL } from "../constants/actions";

const initialState = {
  todos: [],
};

const todos = (state = initialState, action) => {
  switch (action.type) {

    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };

    case DELETE_TODO:
      let updatedTodos = state.todos.filter(
        (todo) => todo.id !== action.payload
      );
      return { ...state, todos: updatedTodos };

    case UPDATE_TODO:
      let updatedTodo = state.todos.filter(
        (todo) => todo.id === action.payload
      );

      var index = state.todos.findIndex(function(item, i){
        return item.id === action.payload
      });

      let newTodos =
        [
          ...state.todos.slice(0,index),
          {...state.todos[index], completed: !state.todos[index].completed},
          ...state.todos.slice(index + 1)
        ];

      return { ...state, todos: newTodos };

      case LIST_SUCCESS:
        return {
          ...state,
          todos: [...state.todos, action.payload],
          message: 'success',
        };

      case LIST_FAIL:
        return {
          ...state,
          message: 'fail',
        };

    default:
      return state;
  }
};

export default todos;