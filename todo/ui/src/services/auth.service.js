import axios from "axios";

const API_URL = "/";

class AuthService {
  login(email, password) {
    return axios
      .post(API_URL + "login", { email, password })
      .then((response) => {
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(name, email, password) {
    console.log(`auth.service.register: ${name}, ${email}, ${password}`)
    return axios
      .post(API_URL + "register", {
        name,
        email,
        password,
      });
  }
}

export default new AuthService();
