import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "/";

class UserService {

  getUser() {
    return axios.get(API_URL + "getByEmail", { headers: authHeader() });
  }

}

export default new UserService();
