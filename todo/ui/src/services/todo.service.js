import axios from "axios";

const API_URL = "/";

class TodoService {
  create(task, user) {
    return axios
      .post(API_URL + "todo", { task, user })
      .then((response) => {
        return response.data;
      });
  }

  list(user) {
    return axios
      .post(API_URL + "todo", { user })
      .then((response) => {
        return response.data;
      });
  }

  get(id, user) {
    return axios
      .post(API_URL + "todo", { id, user })
      .then((response) => {
        return response.data;
      });
  }
}

export default new TodoService();
