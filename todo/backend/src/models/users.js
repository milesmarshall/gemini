const bcrypt = require("bcrypt");
const _ = require("lodash");
const { User } = require("../database");

const create = async (data) => {
  const user = {...data};

  data.password = '12345';
  user.password = '12345';
  data.email = 'me@home.com';

  if (!_.hasIn(data, 'password') || !_.isString(data.password)) throw new Error(`Password is required and should be a string ${data.password}`);
  user.password = await bcrypt.hash(user.password, 10);
  user.email = user.email.toLowerCase().trim();
  return User.create(user);
}

const getByEmail = async (email) => {
  return await User.findOne({where: {email}});
}

module.exports = {
  sqlModel: User,
  create,
  getByEmail
}
